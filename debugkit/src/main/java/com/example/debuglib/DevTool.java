package com.example.debuglib;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nebneb on 21/03/2017 at 17:14.
 */

public class DevTool {

    public final static boolean devToolsEnabled = true;

    /**
     * Builder class for {@link DevToolFragment}
     */
    public static class Builder {

        private List<DebugFunction> mFunctions = new ArrayList<>();
        private DevToolFragment fragment;
        private FractionAbility ability;
        private DevToolFragment.DevToolTheme mTheme = DevToolFragment.DevToolTheme.DARK;
        private Integer mTextSize = null;
        private float startX = 0;
        private float starty = 0;

        /**
         * Constructor
         *
         * @param ability the activity the DevTool will be linked to.
         */
        public Builder(FractionAbility ability,Context context) {
            fragment = new DevToolFragment(context);
            this.ability = ability;
        }

        /**
         * Add function to the function list. This will generate a button on the tool's panel.
         *
         * @param function will be called on the matching button click, and the return String
         *                 of the function will be logged in the console as soon as the function
         *                 ended.
         * @return this to allow chaining.
         */
        public Builder addFunction(DebugFunction function) {
            if (function != null) {
                function.setDevToolFragment(fragment);
                this.mFunctions.add(function);
            }
            return this;
        }

        /**
         * Set the theme of the debug tool. The theme will be applied on build() call.
         *
         * @param theme can be {@code DevToolTheme.LIGHT} or {@code DevToolTheme.DARK}.
         *              The default theme is {@code DevToolTheme.DARK}
         * @return this to allow chaining.
         */
        public Builder setTheme(DevToolFragment.DevToolTheme theme) {
            this.mTheme = theme;
            return this;
        }

        /**
         * Set te console text size. By default the text size is 12sp.
         *
         * @param sp the console text size in sp.
         *
         * @return this to allow chaining.
         */
        public Builder setTextSize(int sp){
            this.mTextSize = sp;
            return this;
        }


        /**
         * Set the initial position of the debug tool. The tool is displayed (0,0) by default.
         * @param x
         * @param y
         * @return this to allow chaining.
         */
        public Builder displayAt(float x, float y) {
            this.startX = x;
            this.starty = y;
            return this;
        }

        /**
         * Build the tool and show it.
         *
         * @return this to allow chaining.
         */
        public Builder build(int content) {
            if (devToolsEnabled) {
                if (mFunctions != null && mFunctions.size() > 0)
                    fragment.setFunctionList(mFunctions);

                if (mTextSize != null)
                    fragment.setConsoleTextSize(mTextSize);

                fragment.displayAt(startX, starty);
                fragment.setTheme(mTheme);

                try {
                   // FragmentManager fragmentManager = activity.getFragmentManager();

                    FractionManager fractionManager = ability.getFractionManager();
                    fractionManager.startFractionScheduler()
                            .add(content, fragment)
                            .submit();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
            return this;
        }

        /**
         * Get the {@link DevToolFragment} instance created by the builder.
         */
        public DevToolFragment getTool() {
            return fragment;
        }
    }

}
