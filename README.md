# Debugkit
DebugKit lib allows you to use a fancy hovering debug tool to trigger some actions directly in the app. This tool is very useful to trigger some event at runtime, and to have a written feedback directly on your testing phone screen.


## Installation:

##### way-1: use package har
```
Packages up your library into a har，and add the har to the directory entry libs,

Add implementation codes to directory entry/gradle:
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

##### way-2: use gradle
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation "com.gitee.archermind-ti:debugkit:1.0.0-beta"

```


## Usage

1.Add Event
```
final DevTool.Builder builder = new DevTool.Builder((FractionAbility) getAbility(), getContext());
builder.addFunction(new DebugFunction() {
            @Override
            public String call() throws Exception {
                log("doing some stuff...");
                doSomeStuff();
                return "Some stuff was done.";
            }
        }).addFunction(new DebugFunction() {
            @Override
            public String call() throws Exception {
                log("doing some other stuff...");
                doSomeStuff();
                return "Some stuff was done.";
            }
        }).addFunction(new DebugFunction("My function") {
            @Override
            public String call() throws Exception {
                log("doing some stuff again and again...");
                doSomeStuff();
                return "This function has a title!";
            }
        });
        //这是一个预先构建的函数，可以清除控制台，也可以在任何函数中调用clear（）。
        .addFunction(new DebugFunction.Clear("Clear"))
        // 这是一个预构建的函数，用于转储共享首选项文件的内容。
        .addFunction(new DebugFunction.DumpSharedPreferences("Shared prefs", PREFS_FILE_NAME));
```
2.Set Style
```
//默认情况下设置DevToolFragment.DevToolTheme.DARK
builder.setTheme(DevToolFragment.DevToolTheme.DARK)
        //默认情况下，可以设置调试工具的初始位置
       .displayAt(100, 100)
       .build();
```


## Result

Here we have 3 debug tools, from top to bottom:


* LIGHT theme with 5 defined functions
* Minified mode (clicking on the top left corner arrow)
* DARK theme with 3 defined functions and the logged text when clicking on each F1, F2 and F3
___
![overview](screenshot/screenshot.jpg)
___
![overview](screenshot/theme_dark.gif)
![overview](screenshot/theme_light.gif)
___

**Have fun!**
