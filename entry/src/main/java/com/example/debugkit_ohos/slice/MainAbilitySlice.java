package com.example.debugkit_ohos.slice;

import com.example.debugkit_ohos.ResourceTable;
import com.example.debuglib.DebugFunction;
import com.example.debuglib.DevTool;
import com.example.debuglib.DevToolFragment;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.Date;

public class MainAbilitySlice extends AbilitySlice {
    private int mTextSize = 12;
    private Slider mSeekbar;
    private DevToolFragment.DevToolTheme mTheme = DevToolFragment.DevToolTheme.DARK;
    private String PREFS_FILE_NAME = "preferences";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        final Button fab = (Button) findComponentById(ResourceTable.Id_fab);
        final Text functionNumber = (Text) findComponentById(ResourceTable.Id_functions_number);
        final Switch themeSpinner = (Switch) findComponentById(ResourceTable.Id_theme_spinner);
        themeSpinner.setStateOffText("DARK");
        themeSpinner.setStateOnText("LIGHT");
        themeSpinner.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            // 回调处理Switch状态改变事件
            @Override
            public void onCheckedChanged(AbsButton button, boolean isChecked) {
                mTheme = isChecked ? DevToolFragment.DevToolTheme.LIGHT : DevToolFragment.DevToolTheme.DARK;
            }
        });
        mSeekbar = (Slider) findComponentById(ResourceTable.Id_seekBar);
        mSeekbar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                functionNumber.setText(Integer.toString(i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        functionNumber.setText(Integer.toString(mSeekbar.getProgress()));
        fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {

                final DevTool.Builder builder = new DevTool.Builder((FractionAbility) getAbility(), getContext());

                if (mSeekbar != null) {
                    for (int i = 0; i < mSeekbar.getProgress(); i++) {
                        builder.addFunction(doSomeStuff());
                    }
                }

                builder.setTextSize(mTextSize)
                        .setTheme(mTheme)
                        .build(ResourceTable.Id_maincontent);
                // After the tool has been built, you can set:
                // builder.getTool().changeConsoleTextSize(mTextSize);
            }
        });

        final DevTool.Builder builder = new DevTool.Builder((FractionAbility) getAbility(), getContext());
        builder.addFunction(new DebugFunction("Do some stuff") {
            @Override
            public String call() throws Exception {
                return "This function has a title";
            }
        })
                .addFunction(new DebugFunction.Clear("Clear"))
                .addFunction(new DebugFunction("Make ShPrf") {
                    @Override
                    public String call() throws Exception {
                        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
                        Preferences editor = databaseHelper.getPreferences(PREFS_FILE_NAME);
                        editor.putString("UpdatedAt", new Date(System.currentTimeMillis()).toString());
                        editor.putBoolean("Key 1", true);
                        editor.putString("Key 2", "value");
                        editor.putString("Key 3", "value 2");
                        editor.flush();
                        return "Preferences file has been created.";
                    }
                }).addFunction(new DebugFunction.DumpSharedPreferences("Shared prefs", PREFS_FILE_NAME));

        builder.setTextSize(mTextSize)
                .displayAt(50, 200)
                .setTheme(mTheme)
                .build(ResourceTable.Id_maincontent);
    }

    private DebugFunction doSomeStuff() {
        return new DebugFunction() {
            @Override
            public String call() throws Exception {
                // Do some kind of really debugging stuff...
                return "Some stuff was done.";
            }
        };
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
